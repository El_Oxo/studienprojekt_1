from flask import Flask, send_from_directory, request
import sys
from flask_cors import CORS
import socket


app = Flask(__name__)
CORS(app)

sys.path.append("C:\VIntDef\VIntDef")
sys.path.append("C:\VIntDef\VIntDef\preprocessing_website")
import preprocessing_website as pw

print(pw)
print(dir(pw))
print(pw.__name__)
#print(socket.gethostbyname_ex(socket.gethostname())[-1][1])

@app.route("/ping")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/', methods=['POST'])
def handle_post():
    print("yes")
    res = pw.main(*request.json)
    return res

@app.route('/static/<path:path>', methods=['GET'])
def send_js(path):
    return send_from_directory('js', path)

#if __name__ == "__main__":
#    app.run()