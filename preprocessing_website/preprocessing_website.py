#!/usr/bin/env python3

#import libraries
import json
import re
import nltk
import pandas as pd
import os
import platform
import sys
from nltk.corpus import stopwords
from textblob.blob import TextBlob

#high_path = os.path.abspath(".") #Deactivated for Deployment.
high_path = "C:\\VIntDef\\VIntDef" #This solution should be Windows only. There is surely a better way to do this.


def loadDataCSV(documents):
    # Reading CSV
    docu = documents
    data = pd.DataFrame()

    # Check Os Platform
    os_Platform = platform.system()

    # Windows
    if os_Platform == "Windows":
        if docu[0]:
            data = pd.concat([pd.read_csv(
                high_path+'\\CSV\\34_DefsOfIntelligence_AGISIsurvey.csv', encoding='cp1252')])
        if docu[1]:
            data = pd.concat([data, pd.read_csv(
                high_path+'\\CSV\\71_DefsOfIntelligence_LeggHutter2007_extendedTable.csv', encoding='cp1252')])
        if docu[2]:
            data = pd.concat([data, pd.read_csv(
                high_path+'\\CSV\\125_humanIntelligence_suggestedDefs_AGISIsurvey.csv', encoding='cp1252')])
        if docu[3]:
            data = pd.concat([data, pd.read_csv(
                high_path+'\\CSV\\213_machineIntelligence_suggestedDefs_AGISIsurvey.csv', encoding='cp1252')])
    # Linux
    elif os_Platform in ["Linux","Darwin"]:
        if docu[0]:
            data = pd.concat([pd.read_csv(
                high_path+'/CSV/34_DefsOfIntelligence_AGISIsurvey.csv', encoding='cp1252')])
        if docu[1]:
            data = pd.concat([data, pd.read_csv(
                high_path+'/CSV/71_DefsOfIntelligence_LeggHutter2007_extendedTable.csv', encoding='cp1252')])
        if docu[2]:
            data = pd.concat([data, pd.read_csv(
                high_path+'/CSV/125_humanIntelligence_suggestedDefs_AGISIsurvey.csv', encoding='cp1252')])
        if docu[3]:
            data = pd.concat([data, pd.read_csv(
                high_path+'/CSV/213_machineIntelligence_suggestedDefs_AGISIsurvey.csv', encoding='cp1252')])

    # Save definitions in Array
    definitions = data['Definition'].values
    # Save Definitions in a array : deftext
    deftext = []   
    for definition in definitions:
        deftext.append(definition)    
    return deftext


def write_json(deftext,stop_words,words,graph,show,count):

    nodes = []  # all nodes
    # the word cluster of the node (has the same position in the array as the node in the nodes array)
    nodes_word_cluster = []
    # the quantity of the node (has the same position in the array as the node in the nodes array)
    nodes_quantity = []

    source = []  # Source of Link
    target = []  # Target of Link
    # word combination (two consecutive words from one definition (consecutive after filtering not in original definition))
    combination = []
    # quantity of combination (same position as source and target have in their array)
    strength_combination = []

    filtered_sentence = []  # words in the definitions without stopwords in lower case (all words devided by , no division in definitions)
    word_cluster = []  # all word clusters
    all_filtered_sentence = [] # words in the definitions without stopwords in lower case (definitions devided by [],[])

    # Contemplation of every single definition
    for definition in deftext:

        # remove Stopword and defining the word cluster
        sentences, cluster = removeStopWords(definition.lower(),stop_words)

        # for word in sentences:
        filtered_sentence.extend(sentences)
        all_filtered_sentence.append(sentences)
        for word in cluster:
            word_cluster.append(word.lower())

    # Counting Quantity of nodes
    for data in filtered_sentence:
        # Word already in nodes Array
        if data in nodes:
            data_index = nodes.index(data)
            # increase quantity
            nodes_quantity[data_index] += 1
        # Word is not in nodes array -> append word, quantity add cluster
        else:
            nodes.append(data)
            nodes_quantity.append(1)
            index = filtered_sentence.index(data)
            nodes_word_cluster.append(word_cluster[index])

    data_index = 0
    # Initialize array to save colors of each node.
    nodes_colors = [''] * len(nodes)
    while data_index < len(nodes):
        if nodes_quantity[data_index] < count or (nodes_word_cluster[data_index].lower() not in words and len(words) > 0):
            del nodes[data_index]
            del nodes_quantity[data_index]
            del nodes_word_cluster[data_index]
            data_index -= 1
        else:
            if nodes_word_cluster[data_index].lower() == "nn":
                nodes_colors[data_index] = '#3182bd'
            elif nodes_word_cluster[data_index].lower() == "nns":
                nodes_colors[data_index] = '#6baed6'
            elif nodes_word_cluster[data_index].lower() == "nnp":
                nodes_colors[data_index] = '#9ecae1'
            elif nodes_word_cluster[data_index].lower() == "nnps":
                nodes_colors[data_index] = '##c6dbef'   
            elif nodes_word_cluster[data_index].lower() == "vb":
                nodes_colors[data_index] = '#E6550D'
            elif nodes_word_cluster[data_index].lower() == "vbp":
                nodes_colors[data_index] = '#fd8d3c'
            elif nodes_word_cluster[data_index].lower() == "vbg":
                nodes_colors[data_index] = '#fdae6b'
            elif nodes_word_cluster[data_index].lower() == "vbz":
                nodes_colors[data_index] = '#FECD53'
            elif nodes_word_cluster[data_index].lower() == "vbn":
                nodes_colors[data_index] = '#FFEB3B'
            elif nodes_word_cluster[data_index].lower() == "vbd":
                nodes_colors[data_index] = '#Fff9c4'
            elif nodes_word_cluster[data_index].lower() == "jj":
                nodes_colors[data_index] = '#31a354'
            elif nodes_word_cluster[data_index].lower() == "jjs":
                nodes_colors[data_index] = '#74c476'
            elif nodes_word_cluster[data_index].lower() == "jjr":
                nodes_colors[data_index] = '#a1d99b'
            elif nodes_word_cluster[data_index].lower() == "rb":
                nodes_colors[data_index] = '#9e9ac8'
            elif nodes_word_cluster[data_index].lower() == "rbr":
                nodes_colors[data_index] = '#bcbddc'
            else:
                nodes_colors[data_index] = 'grey'
        data_index += 1
    


 # select frequency of the twentieth most common node for checkbox "Show only 20 most common words"
    if show == "true" and len(nodes) > 20:
        temp_node_quantity = nodes_quantity.copy()
        temp_node_quantity.sort(reverse=True)
        min_qantity = temp_node_quantity[19]
        data_index = 0
        while data_index < len(nodes):
            if nodes_quantity[data_index] < min_qantity:
                del nodes[data_index]
                del nodes_quantity[data_index]
                del nodes_word_cluster[data_index]
                data_index -= 1
            data_index += 1


    # find word combinations
    # #for each definition
    for filtered_sentence in all_filtered_sentence:
        currentWordIndex = -1

        #for each word in one definition
        for word in filtered_sentence:
            #we look at the next word
            currentWordIndex +=1
            data = word.lower()

            #debug print(data)
            #debug print('currentWordIndex: ' + str(currentWordIndex))

            nextWordFound=False
            # should current word be filtered out
            if data in nodes: 
                # initialise next for new current
                assumedNextWordIndex=currentWordIndex+1 
                # find next word, last word has no next word and is therefore out of range, nextword is max the last one
                while (not nextWordFound and (currentWordIndex < (filtered_sentence.__len__()-1)) and (assumedNextWordIndex <= (filtered_sentence.__len__()-1)) ):
        
                    #next word which might be chosen as next
                    nextWord = filtered_sentence[assumedNextWordIndex] 

                    #debug print('assumedNextW: '+ nextWord)
                    #debug print('assumedNextIndex: ' + str(assumedNextWordIndex))

                    #next word does not equal word before (cant connect the node to itself in the graph)
                    if nextWord != data: 
                        # should it be filtered out
                        if nextWord in nodes: 
                            nextWordFound=True
                            p_combination = data+nextWord  # combining words
                            # combination does already exist, increase quantity
                            if(p_combination in combination):
                                p_combination_index = combination.index(
                                    p_combination)
                                strength_combination[p_combination_index] +=1
                                #debug print('Saved:' + combination[p_combination_index])
                            else:
                                # check combination in different order
                                p_combination2 = nextWord+data
                                if(p_combination2 in combination):
                                    p_combination_index = combination.index(
                                        p_combination2)
                                    strength_combination[p_combination_index] +=1
                                    #debug print('Saved:' + combination[p_combination_index])
                                # if the combination does not exist, add it
                                else:
                                    source.append(data)
                                    target.append(nextWord)
                                    combination.append(p_combination)
                                    strength_combination.append(1)
                                    #debug print('Saved:' + combination[combination.__len__()-1] )          
                    #assumedNextWord is not the next, try next
                    assumedNextWordIndex+=1
 
    #for debug: combination array is not in the right order (same combinations exist only once)

    # -------------------------------------------------------
    # CREATING JSON
    # Json file for graphs
    if (graph):
        with open("current.json", 'w') as outfile:
            json_data = {}
            json_data['nodes'] = []
            x = 0
            # creating word entries
            for data in nodes:
                json_data['nodes'].append({
                    'word': nodes[x],
                    'cluster': nodes_word_cluster[x],
                    'quantity': nodes_quantity[x],
                    'color': nodes_colors[x]
                })
                x = x+1
            # creating link entries
            json_data['links'] = []
            x = 0
            for data in combination:
                json_data['links'].append({
                    'source': source[x].lower(),
                    'target': target[x].lower(),
                    'strength': strength_combination[x],
                })
                x = x+1
            json.dump(json_data, outfile)
            print(json.dumps(json_data))
            return json.dumps(json_data)
    else:
        # Json file for chart
        with open("current.json", 'w') as outfile:
            json_data = []
            x = 0
            # creating word entries
            for data in nodes:
                json_data.append({
                    'word': nodes[x],
                    'cluster': nodes_word_cluster[x],
                    'quantity': nodes_quantity[x],
                    'color': nodes_colors[x]
                })
                x = x+1
            json.dump(json_data, outfile)
            return json.dumps(json_data)


def removeStopWords(definition,stop_words):
    # replacing special signs
    definition = (
        " ".join(re.findall(r"[A-Za-z0-9]*", definition))).replace("  ", " ")
    blob = TextBlob(definition)
    # REMOVING STOP WORDS

    filtered_sentence = []  # words that are in the definitions
    word_cluster = []  # saving the word cluster for every word
    # check every word and identify word cluster
    for word, pos in blob.tags:

        # check if word in stop words
        if word not in stop_words:
            # if not add stop word, add word and cluster to arrays
            filtered_sentence.append(word)
            word_cluster.append(pos)
    return (filtered_sentence, word_cluster)


nouns = ["nn", "nns", "nnp", "nnps"]
verbs = ["vb", "vbd", "vbg", "vbn", "vbp", "vbz"]
adjectives = ["jj", "jjr", "jjs"]
def cluster(all_words):
    # dont worry about this Mistake if word.split() underlined, it runs nevertheless
    word_wants = []
    if all_words[0]:
        word_wants.extend(nouns)
    if all_words[1]:
        word_wants.extend(verbs)
    if all_words[2]:
        word_wants.extend(adjectives)
    return word_wants

def main(graph, show, documents, count, words):
    count = int(count)
    nltk.download('stopwords')
    nltk.download('punkt')
    nltk.download('averaged_perceptron_tagger')

    deftext = loadDataCSV(documents)
    words = cluster(words)

    # define stop words
    stop_words = set(stopwords.words('english'))
    return write_json(deftext,stop_words,words,graph,show,count)

# This is not ideal.
if __name__ == '__main__':
    # read parameters
    print(sys.argv)
    graph = sys.argv[1] == 'true'
    show = sys.argv[2] == 'true'
    documents = [ x == 'true' for x in sys.argv[3].split(',')]
    count = int(sys.argv[4])
    #words = sys.argv[5].split(',')
    words = [x == 'true' for x in sys.argv[5].split(',')]
    main(graph, show, documents, count,words)


