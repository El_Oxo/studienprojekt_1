# Studienprojekt_1

## 1) Installation (Windows) 2022 

``` 
1. create the following folder structure
    C:\VIntDef\VIntDef
    C:\Apache24

2. install Python3 
    https://www.python.org/downloads/
    set the tick while installing for the path variable or add it manually
    open the cmd and try "pip"

    if it doesn't work your installation of python is faulty or not added to path,
    try deinstalling and reinstalling it 

3. install python modules
    open the cmd: 
    pip install
        mod_wsgi
        flask
        flask-cors
        nltk
        pandas
        textblob

4. Configure mod_wsgi to know where the python installation is
    open the cmd: 
    mod_wsgi-express module-config
    save the output somewhere its needed for a config file later on

    if the command doesn't work find out the installation paths manually
    e.g. %appdata%..\local\programs\Python 

5. clone the git repo
    open the cmd:
    navigatee to C:\VIntDef\VIntDef 
    git clone https://gitlab.com/El_Oxo/studienprojekt_1
    delete any additional folder like "studienprojekt1" so that all the files and 
    folders like "static" are under C:\VIntDef\VIntDef

6. install apache
    install apache into C:\Apache24
    https://www.apachelounge.com/download/
    again delete any additional folder so that all files and folders (bin,logs,conf)
    lie under C:\Apache24

    install vc redist
    https://www.apachelounge.com/download/
    if asked or needed restart the pc

7. configure Apache
    open the httpd.conf in C:\Apache24\conf
    after all the "load modules", roundabout line 168 add your output from step 4.
    e.g. 
        LoadFile "d:/programme (x86)/python/python38.dll"
	    LoadModule wsgi_module "d:/programme (x86)/python/lib/site-packages/mod_wsgi/server/mod_wsgi.cp38-win_amd64.pyd"
	    WSGIPythonHome "d:/programme (x86)/python"
    after line 232 add (this line might not be necessary)
        ServerName www.VIntDef.com:80 
    add at the end of the file (about line 542)
        # Include Flask file
	    Include conf/VIntDef.conf
    save
    
    create a file called VIntDef.conf in C:\Apache24\conf
    add and save the following
        #WSGIPythonPath "C:/VIntDef"
        <VirtualHost *:80>
            ServerName www.VIntDef.com:80
            WSGIScriptAlias / C:\VIntDef\VIntDef\wsgi_scripts\VIntDef.wsgi application-group=%{GLOBAL}
            <Directory "C:/VIntDef">
                AllowOverride None 
                Require all granted
            </Directory>
        </VirtualHost>

8. start the project
    open the cmd and navigate to C:Apashe/bin ,execute
    httpd.exe

    open localhost/static/html_script.html in your browser
    be sure not to use https but http
    see step 10 for common issues

    exit by pressing strg c in the cmd (press often)

9. access the website on another device in the same wifi
    in cmd of the pc:
    ipconfig
    copy the ip and replace localhost in the adress
    e.g. 192.162.3.1/static/html_script.html 

    this works for the hwr and some home installations - issues might be router settings
    this is just temporarily necessary for the hwr

10. common issues/help
    clicking on the hexagon buttons doesn't open a modal
        in the project folder go to the html_script.html
        change the paths (about line 13-21) from ../ to ./
    tested IDE: Visual Studio Code
```

## 2) Installation Pre2022  Linux (Ubuntu & Debian)
1. Install Python 3.x.x from https://www.python.org/downloads/windows/
2. Install nodejs from https://nodejs.org/de/download/ and add it to the PathVariables
3. Clone the Repository 
    -> git clone https link for git Repo
4. Open the Project Folder E. g. with VsCode
5. Install npm
    -> sudo apt install npm
6. Install all necessary modules with npm using the comand
    -> npm -i
7. Install Pyhon_Modules usind pip
````bash
pip install -r requirements.txt
```
8. Alternativly usa a virtual environment
   1. Install virtualenv with `pip install virtualenv`
   2. Create a new virtualenv with
	-> virtualenv -p python3 project
   3. Activate the environment with
	-> source project/bin/activate
   4 cd into Project dir and install packages with
	-> pip install -r requirements.txt

## 2) Installation Windows 10
The commands are similar. Simply google the single commands for cmd

## 3) If npm -i doesn't install the necessary modules automatically
Install all necessary modules manually using npm
    1. index.js
        -> npm install index.js
    2. cors
        -> npm install cors
    3. express
        -> npm install express
    4. pip
        -> npm install pip
    5. pyhton-shell
        -> npm install pyhton-shell
    6. body-parser
        -> npm install body-parser
    7. file-system
        -> npm install file-system
    8. Pyhon_Modules using pip as above 

## 4) Notes:
1. Test with name –version if modules or software are alrady installed
       e.g. node –version
2.  Maybe you have to use pip3 for pyhton3
       e.g. pip3 install nltk
3. Commands above works with Linux Debian and Ubuntu

## 5) Running

1. Open a Terminal, navigate into the Project Folder.
2. Start the Server -> node index.js
3. Open the HTML_Script.html File with a Browser.

### Build and run docker image

Build with
````bash
docker build -t somename . 
```
and run with
````bash
docker run -p 3000:3000 somename
```

with flask: (late 2021)
start the project: 
1.
(Windows) venv\Scripts\activate 
(Mac) go to folder of virtual env -> source bin/activate
2.
(Windows) set FLASK_APP=__init__.py
(Mac) export FLASK_APP=__init__.py
(older versions replace __init__.py with __main__.py)
3.
flask run
4.
open in browser: 
http://localhost:5000/static/HTML_Script.html
5. 
exit with
venv\Scripts\deactivate 

