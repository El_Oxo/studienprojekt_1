var express = require('express');
var app = express();
const fs = require('fs');
var cors = require('cors');
var bodyParser = require('body-parser');
var pythonShell = require('python-shell');
var finished_py = false;

app.use(bodyParser.json());

//enable cors
app.use(cors({
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
}));

app.post('/', function (req, res) {
    finished_py = false;
    var body = req.body;
    pythonShell.PythonShell.run('preprocessing_website.py', { args: body }, function (err) {
        if (err) throw err;
        console.log('finished preprocessing script');
        finished_py = true;
    });

    res.send('true');
});
app.get('/', function (req, res) {
    //send variable to check if python script is already finished
    if (finished_py) {
        var jsonString;
        jsonString = fs.readFileSync("current.json", 'utf8');
        res.send(jsonString);
    } else {
        res.send(finished_py);
    }
});
app.listen(3000, function () {
    console.log('VintDef listening on port 3000');
});