FROM python:3-buster
COPY . .
RUN pip install -r requirements.txt
CMD flask run